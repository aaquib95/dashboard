const ConfigObj = {
  chart: {
    height: 380,
    width: 900,
    type: "bar",
  },
  title: {
    text: "",
  },
  xAxis: {
    categories: [
      "SHORT VIDEO",
      "MOBILE PAGE",
      "WEB DESIGN",
      "TAXI HAILING",
      "ILLUSTRATIONS",
      "ILLUSTRATIONS",
    ],
  },
  yAxis: {
    min: 0,
    max: 15,
  },
  legend: {
    enabled: false,
  },
  plotOptions: {
    series: {
      stacking: "normal",
    },
  },
  series: [
    {
      name: "John",
      data: [5, 3, 4, 7, 2],
    },
    {
      name: "Jane",
      data: [2, 2, 3, 2, 1],
    },
    {
      name: "Joe",
      data: [3, 4, 4, 2, 5],
    },
  ],
  //   chart: {
  //     type: "columnrange",
  //     inverted: true,
  //   },

  //   accessibility: {
  //     description:
  //       "Image description: A column range chart compares the monthly temperature variations throughout 2017 in Vik I Sogn, Norway. The chart is interactive and displays the temperature range for each month when hovering over the data. The temperature is measured in degrees Celsius on the X-axis and the months are plotted on the Y-axis. The lowest temperature is recorded in March at minus 10.2 Celsius. The lowest range of temperatures is found in December ranging from a low of minus 9 to a high of 8.6 Celsius. The highest temperature is found in July at 26.2 Celsius. July also has the highest range of temperatures from 6 to 26.2 Celsius. The broadest range of temperatures is found in May ranging from a low of minus 0.6 to a high of 23.1 Celsius.",
  //   },

  //   title: {
  //     text: "Temperature variation by month",
  //   },

  //   subtitle: {
  //     text: "Observed in Vik i Sogn, Norway, 2017",
  //   },

  //   xAxis: {
  //     categories: [
  //       "Jan",
  //       "Feb",
  //       "Mar",
  //       "Apr",
  //       "May",
  //       "Jun",
  //       "Jul",
  //       "Aug",
  //       "Sep",
  //       "Oct",
  //       "Nov",
  //       "Dec",
  //     ],
  //   },

  //   yAxis: {
  //     title: {
  //       text: "Temperature ( °C )",
  //     },
  //   },

  //   tooltip: {
  //     valueSuffix: "°C",
  //   },

  //   plotOptions: {
  //     columnrange: {
  //       dataLabels: {
  //         enabled: true,
  //         format: "{y}°C",
  //       },
  //     },
  //   },

  //   legend: {
  //     enabled: false,
  //   },

  //   series: [
  //     {
  //       name: "Temperatures",
  //       data: [
  //         [-9.9, 10.3],
  //         [-8.6, 8.5],
  //         [-10.2, 11.8],
  //         [-1.7, 12.2],
  //         [-0.6, 23.1],
  //         [3.7, 25.4],
  //         [6.0, 26.2],
  //         [6.7, 21.4],
  //         [3.5, 19.5],
  //         [-1.3, 16.0],
  //         [-8.7, 9.4],
  //         [-9.0, 8.6],
  //       ],
  //     },
  //   ],
};

export default ConfigObj;
