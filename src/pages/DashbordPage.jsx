import React, { useEffect } from "react";
import { Line } from "react-chartjs-2";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import ConfigObj from "./utils/ConfigObj";

const DashbordPage = () => {
  useEffect(() => {
    document.title = "dashboard";
  }, []);

  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-2 dash-left">
            <div className="row align-items-center redesign">
              <img className="logo-left" src="/images/logo.png" alt="logo" />
              <div>Redesign </div>
            </div>
            <div className="row mt-4 ml-2 mr-2 frannnk justify-content-center">
              <div>Frannnk was number one on the day</div>
            </div>
            <div className="row mt-4 menu-tab">
              <div>
                <span className="dot "></span>Menu
              </div>
            </div>

            <div className="row ml-3 mt-3 menu-items justify-content-center">
              <div className="col-3 d-flex justify-content-center">
                <div>
                  <i className="fas fa-home align-items-center"></i>
                </div>
              </div>
              <div className="col-9 justify-content-left">
                <div>Home page</div>
              </div>
            </div>

            <div className="row ml-3 mt-3 menu-items justify-content-center">
              <div className="col-3 d-flex justify-content-center">
                <div>
                  <i className="fas fa-store align-items-center"></i>
                </div>
              </div>
              <div className="col-9 justify-content-left">
                <div>Team data</div>
              </div>
            </div>

            <div className="row align-items-center menu-items team-cl ">
              <div className="col-3">
                <div>
                  <i className="fas fa-folder "></i>
                </div>
              </div>
              <div className="col-9">
                <div className="team-project">Team project</div>
              </div>
            </div>

            <div className="row ml-3 mt-3 menu-items justify-content-center">
              <div className="col-3 d-flex justify-content-center">
                <div>
                  <i className="fas fa-folder-plus align-items-center"></i>
                </div>
              </div>
              <div className="col-9 justify-content-left">
                <div>Team collect</div>
              </div>
            </div>
            <div className="row mb-0 justify-content-center">
              <hr className="column-line-1" />
            </div>
            <div className="row align-items-center menu-tab">
              <span className="dot-2 "></span>Skills
            </div>

            <div className="row mt-3">
              {" "}
              <div className="col-9 justify-content-center">
                {" "}
                <div className="ml-3">ui/ux design</div>{" "}
              </div>{" "}
              <div className="col-3 align-items-left">
                <div className="number-radius">178</div>
              </div>
            </div>
            {/* <div className="row">web design</div> */}
            <div className="row mt-2">
              {" "}
              <div className="col-9 justify-content-center">
                {" "}
                <div className="ml-3">web design</div>{" "}
              </div>{" "}
              <div className="col-3 align-items-left">
                <div className="number-radius">354</div>
              </div>
            </div>
            {/* <div className="row">illustrations</div> */}
            <div className="row mt-2">
              {" "}
              <div className="col-9 justify-content-center">
                {" "}
                <div className="ml-3">illustrations</div>{" "}
              </div>{" "}
              <div className="col-3 align-items-left">
                <div className="number-radius">78</div>
              </div>
            </div>
            {/* <div className="row">Dynamic effect</div> */}
            <div className="row mt-2">
              {" "}
              <div className="col-9 justify-content-center">
                {" "}
                <div className="ml-3">Dynamic effect</div>{" "}
              </div>{" "}
              <div className="col-3 align-items-left">
                {" "}
                <div className="number-radius">132</div>
              </div>
            </div>
            <div className="row mb-0 mt-2 justify-content-center">
              <div className="col-11 ">
                <div className="column-line-2"> ... ...</div>
              </div>
            </div>

            <div className="row mt-4 border-das-bottom ">
              <div className="col-3 mt-2">
                <label className="toggle">
                  <input
                    className="toggle__input"
                    name
                    type="checkbox"
                    id="myToggle"
                  />
                  <div className="toggle__fill-2" />
                </label>
              </div>
              <div className="col-9 mt-2">Sun / Moon</div>
            </div>
          </div>
          <div className="col-10 card-container dash-right">
            <div className="row right-nav-row justify-content-center">
              <div className="col-7">
                <div className="form-group has-search">
                  <span className="fa fa-search form-control-feedback" />
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search for the members and works... ..."
                  />
                </div>
              </div>
              <div className="col-1 col-md-1-5 bell-cog">
                <i className="fas fa-bell">
                  <span class="dot-3"></span>
                </i>
              </div>
              <div className="col-1 bell-cog-2">
                <i className="fas fa-cog"></i>
              </div>
              <div className="col-2 ">
                <div className="never-stop">
                  <img src="/images/avtar.jpeg" alt="" className="avatar" />
                  <span class="dot-4"></span>
                  Never_stop
                </div>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="card">
                <div className="card-body">
                  <p className="card-text">SHORT VIDEO</p>
                  <p className="card-text-2">Leader</p>
                </div>

                <img
                  className="avatar-2"
                  src="/images/1.jpg"
                  alt="Card image cap"
                />
              </div>
              <div className="card">
                <div className="card-body">
                  <p className="card-text">MOBILE PAGE</p>
                  <p className="card-text-2">Leader</p>
                </div>
                <img
                  className="avatar-2"
                  src="/images/2.jpeg"
                  alt="Card image cap"
                />
              </div>
              <div className="card">
                <div className="card-body">
                  <p className="card-text">WEB DESIGN</p>
                  <p className="card-text-2">Leader</p>
                </div>
                <img
                  className="avatar-2"
                  src="/images/3.png"
                  alt="Card image cap"
                />
              </div>
              <div className="card">
                <div className="card-body">
                  <p className="card-text"> TAXI-HAILING </p>
                  <p className="card-text-2">Leader</p>
                </div>
                <img
                  className="avatar-2"
                  src="/images/4.png"
                  alt="Card image cap"
                />
              </div>
              <div className="card">
                <div className="card-body">
                  <p className="card-text">ILLUSTRATIONS</p>
                  <p className="card-text-2">Leader</p>
                </div>
                <img
                  className="avatar-2"
                  src="/images/5.png"
                  alt="Card image cap"
                />
              </div>
            </div>
            <div className="row mt-3 align-items-center date-row status-container border-c ">
              <div className="col-3 ">
                <div className="month-text d-flex align-items-center">
                  <div> April 3, 2019</div> <i class="fas fa-caret-right"></i>
                </div>
              </div>
              <div className="col-8 ">
                <div className="list-date">
                  <div className="date-wise">Day</div>
                  <span className="vl"></span>
                  <div className="date-wise">Week</div>
                  {/* <div className="date-wise">|</div> */}
                  <span className="vl"></span>
                  <div className="date-wise ">Month</div>
                  <span className="vl"></span>
                  <div className="date-wise">Year</div>
                </div>
              </div>
              <div className="col-1 plus-icon"> + </div>
            </div>
            <div className="row status-container">
              <HighchartsReact highcharts={Highcharts} options={ConfigObj} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashbordPage;
