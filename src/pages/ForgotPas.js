import React from "react";
import "./forgot.css";
import { Formik, Form, Field } from "formik";

function validateEmail(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else if (isNaN(value)) {
    console.log("in email");
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      error = "Invalid email address";
    }
  } else {
    console.log("in mobile", value, typeof value);
    if (value.length !== 10) {
      console.log("length", value.length);
      error = "Mobile Length Should Be 10 Digit.";
    }
  }

  return error;
}

const ForgotPas = () => {
  //   const redirectOtp = () => {
  //     props.history.push("Otp");
  //   };
  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div className="row justify-content-center mt-4">
              <p>Forgot Password</p>
            </div>

            <Formik
              initialValues={{
                email: "",
              }}
              onSubmit={values => {
                // same shape as initial values
                console.log(values);
              }}
            >
              {({ errors, touched, isValidating }) => (
                <div className="row justify-content-center ">
                  <Form className="form-11">
                    <div className="row lbl-11">
                      <label htmlFor="email">Email or Mobile </label>
                    </div>
                    <div className="row">
                      <Field
                        name="email"
                        type="text"
                        placeholder="Enter your email or mobile number"
                        validate={validateEmail}
                        className={`inp ${
                          errors.email && touched.email && "error"
                        }`}
                      />
                    </div>
                    <div className="row err-11">
                      {errors.email && touched.email && (
                        <div className="input-feedback">{errors.email}</div>
                      )}
                    </div>
                    <div className="row">
                      <button type="submit">Submit</button>
                    </div>
                  </Form>
                </div>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ForgotPas;
