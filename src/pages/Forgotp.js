import React, { useState } from "react";
import { Formik, Form, Field } from "formik";
import { NavLink } from "react-router-dom";
// import "./front.css";
// import "./forgot.css";

function validateEmail(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else if (isNaN(value)) {
    console.log("in email");
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      error = "Invalid email address";
    }
  } else {
    console.log("in mobile", value, typeof value);
    if (value.length !== 10) {
      console.log("length", value.length);
      error = "Mobile Length Should Be 10 Digit.";
    }
  }
  return error;
}

const Forgotp = () => {
  const [error, setError] = useState(false);
  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-4 left">
            <div className="myLeftCtn">
              <div className="row">
                <div className="col-6">
                  <div className="uldesign">
                    <div className="row">
                      <ul className="header">Follow</ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-facebook ggg"></i> Facebook
                      </ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-twitter ggg"></i> Twitter
                      </ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-instagram ggg"></i> Instagram
                      </ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-linkedin ggg"></i> LinkedIn
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="col-6 ">
                  <div className="uldesign-2">
                    <NavLink to="/" className="forgotPage ">
                      Sign in
                    </NavLink>
                    <NavLink to="/Forgotp" className=" forgotPage">
                      Sign up
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-8 right">
            <div className="row forgot-password-container">
              <div className="col-12">
                <div className="row justify-content-center ">
                  <p className="mb-4">Forgot Password</p>
                </div>

                <Formik
                  initialValues={{
                    email: "",
                  }}
                  onSubmit={values => {
                    // same shape as initial values
                    console.log(values);
                  }}
                >
                  {({ errors, touched, isValidating }) => (
                    <div className="row justify-content-center ">
                      <Form className="form-12">
                        <div className="col-12">
                          <div className="input-group mb-3 ">
                            <div className="input-group-prepend ">
                              <span
                                // className="input-group-text error bg-white inpb"
                                className={`input-group-text bg-white inpb  ${
                                  errors.email && touched.email && "error"
                                }`}
                                id="basic-addon1"
                              >
                                <i className="fas fa-user " />
                              </span>
                            </div>

                            <Field
                              name="email"
                              type="email"
                              placeholder="Email or Mobile"
                              validate={validateEmail}
                              className={`form-control inpb ${
                                errors.email && touched.email && "error"
                              }`}
                            />
                          </div>
                          <div className="row">
                            <div className="col-12 mb-1">
                              {errors.email && touched.email && (
                                <div className="input-feedback">
                                  {errors.email}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="col-12 ">
                          <button
                            className="btn btn-block border-0 butt"
                            type="submit"
                          >
                            Submit
                          </button>
                        </div>
                      </Form>
                    </div>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Forgotp;
