import React, { useState } from "react";
import { Formik, Form, Field } from "formik";
import "./front.css";
import "./forgot.css";
import { NavLink } from "react-router-dom";
function validateEmail(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else if (isNaN(value)) {
    console.log("in email");
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      error = "Invalid email address";
    }
  } else {
    console.log("in mobile", value, typeof value);
    if (value.length !== 10) {
      console.log("length", value.length);
      error = "Mobile Length Should Be 10 Digit.";
    }
  }
  return error;
}

function validatePassword(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else {
    console.log("in email");
    if (!/(?=.*[0-9])/.test(value)) {
      error = "Invalida password. Must contain one number";
    } else if (value.length < 8) {
      error = "Password must be 8 characters long.";
    }
  }
  return error;
}

const Simple = props => {
  const redirectForgotp = () => {
    props.history.push("/forgot-password");
  };
  const [error, setError] = useState(false);
  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-4 left">
            <div className="myLeftCtn">
              <div className="row">
                <div className="col-6">
                  <div className="uldesign">
                    <div className="row">
                      <ul className="header">Follow</ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-facebook ggg"></i> Facebook
                      </ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-twitter ggg"></i> Twitter
                      </ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-instagram ggg"></i> Instagram
                      </ul>
                    </div>
                    <div className="row">
                      <ul>
                        <i className="fab fa-linkedin ggg"></i> LinkedIn
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="col-6 ">
                  <div className="uldesign-2">
                    <NavLink to="/" className=" " activeClassName="activelink">
                      Sign in
                    </NavLink>
                    <NavLink
                      to="/Forgotp"
                      className=" "
                      activeClassName="activelink"
                    >
                      Sign up
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-8 right">
            <div className="card-wrapper">
              <div class="card cutome-card">
                <div className="row">
                  <div class="card-body">
                    <Formik
                      initialValues={{
                        password: "",
                        email: "",
                      }}
                      onSubmit={values => {
                        // same shape as initial values
                        console.log(values);
                      }}
                      //   validationSchema={validateEmail}
                    >
                      {({ errors, touched, isValidating }) => (
                        <Form className="form-11">
                          <div className="col-12">
                            <div className="input-group mb-3 ">
                              <div className="input-group-prepend ">
                                <span
                                  // className="input-group-text error bg-white inpb"
                                  className={`input-group-text bg-white inpb  ${
                                    errors.email && touched.email && "error"
                                  }`}
                                  id="basic-addon1"
                                >
                                  <i className="fas fa-user " />
                                </span>
                              </div>

                              <Field
                                name="email"
                                type="email"
                                placeholder="Email or Mobile"
                                validate={validateEmail}
                                className={`form-control inpb ${
                                  errors.email && touched.email && "error"
                                }`}
                              />
                            </div>
                            <div className="row">
                              <div className="col-12 mb-1">
                                {errors.email && touched.email && (
                                  <div className="input-feedback">
                                    {errors.email}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="col-12">
                            <div className="input-group mb-4">
                              <div className="input-group-prepend">
                                <span
                                  // className="input-group-text error bg-white inpb"
                                  className={`input-group-text bg-white inpb ${
                                    errors.password &&
                                    touched.password &&
                                    "error"
                                  }`}
                                  id="basic-addon1"
                                >
                                  <i className="fas fa-lock" />
                                </span>
                              </div>

                              <Field
                                name="password"
                                type="password"
                                placeholder="Password"
                                validate={validatePassword}
                                className={`form-control inpb ${
                                  errors.password && touched.password && "error"
                                }`}
                              />

                              <div className="col-12 mb-1">
                                {errors.password && touched.password && (
                                  <div className="input-feedback">
                                    {errors.password}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="col-12 ">
                            <button
                              className="btn btn-block border-0 butt"
                              type="submit"
                            >
                              Sign in
                            </button>
                          </div>
                          <div className="row mt-2 mb-0 mt-3 align-items-center">
                            <div className="col-5 pr-0 mt-1">
                              <hr />
                            </div>
                            <div className="col-2 ml-1, mr-1, 0 ">o</div>
                            <div className="col-5 ml-0 pl-0 mt-1">
                              <hr />
                            </div>
                          </div>

                          <div className="row link-11">
                            <span className="psw">
                              Forgot{" "}
                              <a href="" onClick={redirectForgotp}>
                                password?
                              </a>
                            </span>
                          </div>
                        </Form>
                      )}
                    </Formik>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Simple;
