import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

function validateEmail(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else if (isNaN(value)) {
    console.log("in email");
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      error = "Invalid email address";
    }
  } else {
    console.log("in mobile", value, typeof value);
    if (value.length !== 10) {
      console.log("length", value.length);
      error = "Mobile Length Should Be 10 Digit.";
    }
  }
  return error;
}

function validatePassword(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else {
    console.log("in email");
    if (!/(?=.*[0-9])/.test(value)) {
      error = "Invalida password. Must contain one number";
    } else if (value.length < 8) {
      error = "Password must be 8 characters long.";
    }
  }
  return error;
}

const Login = () => {
  return (
    <div>
      <div className="container">
        <div className="myCard">
          <div className="row">
            <div className="col-md-aa">
              <div className="myLeftCtn">
                <div className="box row px-0">
                  <div className="col-6">
                    <header>Follow</header>

                    <div className="uldesign">
                      <ul>
                        <i className="fab fa-github ggg"></i> Github
                      </ul>
                      <ul>
                        <i className="fab fa-twitter ggg"></i> Twitter
                      </ul>
                      <ul>
                        <i className="fab fa-behance ggg"></i> Behance
                      </ul>
                      <ul>
                        <i className="fas fa-basketball-ball ggg"></i> Dribble
                      </ul>
                    </div>
                  </div>

                  <div className="col-6 ">
                    <div className="uldesign-2">
                      <p className="p-1 ">Login</p>
                      <p className="p-2 ">Sign in</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Formik
              initialValues={{
                password: "",
                email: "",
              }}
              onSubmit={values => {
                // same shape as initial values
                console.log(values);
              }}
            >
              {({ errors, touched, isValidating }) => (
                <div className="col-md-6">
                  <div className="myRightCtn">
                    <Form className="myForm text-center needs-validation novalidate">
                      <h1>Zeit</h1>
                      <div className="input-group ml-0 has-validation">
                        <span
                          className="input-group-text bg-white"
                          id="basic-addon1"
                        >
                          <i className="fas fa-envelope " />
                        </span>
                        <Field
                          // type="email"
                          // className="form-control"
                          // placeholder="Email"
                          // aria-label="Email"
                          // aria-describedby="basic-addon1"
                          // required
                          name="email"
                          type="text"
                          placeholder="Email or mobile number"
                          validate={validateEmail}
                          className={`form-control ${
                            errors.email && touched.email && "error"
                          }`}
                        />
                        {errors.email && touched.email && (
                          <div className="input-feedback">{errors.email}</div>
                        )}
                      </div>
                      <div className="form-group">
                        <div className="input-group mb-3 has-validation">
                          <span
                            className="input-group-text bg-white"
                            id="basic-addon1"
                          >
                            <i className="fas fa-lock" />
                          </span>

                          <Field
                            //   className="form-control"
                            //   type="password"
                            //   id="password"
                            //   placeholder="Password"
                            //   required
                            // />
                            name="password"
                            type="password"
                            placeholder="Password"
                            validate={validatePassword}
                            className={`form-control ${
                              errors.password && touched.password && "error"
                            }`}
                          />
                          {errors.password && touched.password && (
                            <div className="input-feedback">
                              {errors.password}
                            </div>
                          )}
                        </div>
                      </div>
                      <button type="submit" className="butt">
                        Get started
                      </button>
                      <div className="row mt-3 mb-0 align-items-center">
                        <div className="col-5 pr-0">
                          <hr />
                        </div>
                        <div className="col-2 mx-0 ">o</div>
                        <div className="col-5 pl-0">
                          <hr />
                        </div>
                      </div>
                      <div className="social">
                        <p className="face">Facebook</p>
                        <p className="goog">Google +</p>
                      </div>
                    </Form>
                  </div>
                </div>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
