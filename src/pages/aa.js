import React from "react";
import { Formik, Form, Field } from "formik";

function validateEmail(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else if (isNaN(value)) {
    console.log("in email");
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      error = "Invalid email address";
    }
  } else {
    console.log("in mobile", value, typeof value);
    if (value.length !== 10) {
      console.log("length", value.length);
      error = "Mobile Length Should Be 10 Digit.";
    }
  }
  return error;
}

function validatePassword(value) {
  // value = Number(value)
  console.log(value);
  let error;
  if (!value) {
    error = "Required";
  } else {
    console.log("in email");
    if (!/(?=.*[0-9])/.test(value)) {
      error = " Must contain one number";
    } else if (value.length < 8) {
      error = "Password must be 8 characters long.";
    }
  }
  return error;
}

const Aa = () => {
  return (
    <div>
      <div className="myCard login-page">
        <div className="row">
          <div className="col-md-aa">
            <div className="myLeftCtn">
              <div className="box row px-0">
                <div className="col-6">
                  <header>Follow</header>

                  <div className="uldesign">
                    <ul>
                      <i className="fab fa-github ggg"></i> Github
                    </ul>
                    <ul>
                      <i className="fab fa-twitter ggg"></i> Twitter
                    </ul>
                    <ul>
                      <i className="fab fa-behance ggg"></i> Behance
                    </ul>
                    <ul>
                      <i className="fas fa-basketball-ball ggg"></i> Dribble
                    </ul>
                  </div>
                </div>

                <div className="col-6 ">
                  <div className="uldesign-2">
                    <p className="p-1 ">Sign in</p>
                    <p className="p-2 ">Sign up</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Formik
            initialValues={{
              password: "",
              email: "",
            }}
            onSubmit={values => {
              // same shape as initial values
              console.log(values);
            }}
          >
            {({ errors, touched, isValidating }) => (
              <div className="col-md-6">
                <div className="myRightCtn">
                  <Form className="myForm text-center ">
                    <div className="row h111">
                      <h1>Zeit</h1>
                    </div>
                    <div className="row">
                      <div className="input-group">
                        <span
                          className="input-group-texte border-0 bg-white"
                          id="basic-addon1"
                        >
                          <i className="fas fa-envelope " />
                        </span>
                        <div className="row">
                          <Field
                            name="email"
                            type="text"
                            placeholder="Email or mobile number"
                            validate={validateEmail}
                            className={`form-control ${
                              errors.email && touched.email && "error"
                            }`}
                          />
                        </div>
                        <div>
                          {errors.email && touched.email && (
                            <div className="input-feedback">{errors.email}</div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="input-group ">
                        <span
                          className="input-group-text border-0 bg-white"
                          id="basic-addon1"
                        >
                          <i className="fas fa-lock" />
                        </span>

                        <Field
                          name="password"
                          type="password"
                          placeholder="Password"
                          validate={validatePassword}
                          className={`form-control, border-0 ${
                            errors.password && touched.password && "error"
                          }`}
                        />
                        {errors.password && touched.password && (
                          <div className="input-feedback">
                            {errors.password}
                          </div>
                        )}
                      </div>
                    </div>

                    <div className="row">
                      <button type="submit" className="butt">
                        Get started
                      </button>
                    </div>
                    <div className="row mt-3 mb-0 align-items-center">
                      <div className="col-5 pr-0">
                        <hr />
                      </div>
                      <div className="col-2 mx-0 ">o</div>
                      <div className="col-5 pl-0">
                        <hr />
                      </div>
                    </div>
                    <div>
                      <p className="face ">Facebook</p>
                      <p className="goog ">Google +</p>
                    </div>
                  </Form>
                </div>
              </div>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
};

export default Aa;
