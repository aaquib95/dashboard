import React, { useEffect } from "react";
import { NavLink } from "react-router-dom";

const Profile = () => {
  useEffect(() => {
    document.title = "account";
  }, []);
  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-3 left-container">
            <div className="row mt-5 justify-content-center">
              <img
                src="https://www.tutorialspoint.com/bootstrap/images/64.jpg"
                className="rounded-circle user-image-circle"
                alt="Rounded Image"
                width="100"
                height="100"
              />
            </div>
            <div className="row mt-2 justify-content-center">
              <h4 className="user-name">Jessie Lane</h4>
            </div>
            <div className="row justify-content-center">
              <h6 className="user-photolink">(Click to change your photo)</h6>
            </div>
            <div className="row mb-0 mt-4 justify-content-center">
              <div className="col-11 ">
                <hr className="column-line" />
              </div>
            </div>

            <div className="row list-container mb-4 mt-4 border-checker">
              <NavLink
                to="/"
                className=" "
                activeClassName="active-account-link"
              >
                Account
              </NavLink>
            </div>
            <div className="row list-container mb-4">Security & privacy</div>
            <div className="row list-container mb-4">Mobile</div>
            <div className="row list-container mb-4">Find friends</div>
            <div className="row list-container mb-4">History</div>
          </div>
          <div className="col-9">
            <div className="row">
              <div className="col-6 right1-container">
                <div className="row ml-5 mt-4 border-check">
                  <header>Account settings</header>
                </div>
                <div className="row ml-5 mt-4 align-items-center border-check">
                  {/* <label className="border-check-1">Username</label> */}
                  <div className="border-check-1">Username</div>
                  <input
                    name="username"
                    type="text"
                    className="form-control col-lg-7 user-margin "
                    placeholder="Enter your user name"
                  />
                </div>
                <div className="row ml-5 mt-3 mb-5 align-items-center border-check">
                  {/* <label className="border-check-1">Email</label> */}
                  <div className="border-check-1">Email</div>
                  <input
                    name="email"
                    type="email"
                    className="form-control col-lg-7 email-margin"
                    placeholder="Enter your email"
                  />
                </div>
                <div className="row ml-5 mt-5 align-items-center border-check">
                  {/* <label className="border-check-1">Gender</label> */}
                  <div className="border-check-1">Gender</div>
                  <select
                    name="gender"
                    className="form-control col-lg-7 gender-margin"
                  >
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                </div>
                <div className="row ml-5 mt-3 align-items-center border-check">
                  {/* <label>Birthday</label> */}
                  <div className="border-check-1">Birthday</div>
                  <input
                    name="birthday"
                    type="date"
                    className="form-control col-lg-7 birthday-margin"
                  />
                </div>
                <div className="row ml-5 mt-3 mb-5 align-items-center border-check ">
                  {/* <label>Job</label> */}
                  <div className="border-check-1">Job</div>
                  <input
                    name="job"
                    type="text"
                    className="form-control col-lg-7 job-margin "
                    placeholder="Enter your job profile"
                  />
                </div>
                <div className="row ml-5 mt-3 align-items-center border-check">
                  {/* <label>Language</label> */}
                  <div className="border-check-1">Langauge</div>
                  <select
                    name="langauge"
                    class="form-control col-lg-7 language-margin"
                  >
                    <option>English</option>
                    <option>Hindi</option>
                  </select>
                </div>
                <div className="row ml-5 mt-3 align-items-center border-check">
                  {/* <label>Country</label> */}
                  <div className="border-check-1">Country</div>
                  <select
                    name="country"
                    class="form-control col-lg-7 country-margin"
                  >
                    <option>India</option>
                    <option>Japan</option>
                  </select>
                </div>
                <div className="row ml-5 mt-3 align-items-center border-check">
                  {/* <label>City</label> */}
                  <div className="border-check-1">City</div>
                  <select name="city" class="form-control col-lg-7 city-margin">
                    <option>Delhi</option>
                    <option>Bangalore</option>
                  </select>
                </div>
              </div>
              {/* <div className="col-1 middle-container">
            <div className="row middle-content">
              <hr />
            </div>
          </div> */}
              <div className="col-6 mt-4 right2-container">
                <div className="row mt-3 align-items-center ml-4 border-check">
                  {/* <label>Current Password</label> */}
                  <div className="border-check-1">Current Password</div>
                  <input
                    name="currentpassword"
                    type="password"
                    className="form-control col-lg-7 current-margin "
                    placeholder="Enter current password"
                  />
                </div>
                <div className="row mt-3 align-items-center ml-4 border-check">
                  {/* <label>New Password</label> */}
                  <div className="border-check-1">New Password</div>
                  <input
                    name="newpassword"
                    type="password"
                    className="form-control col-lg-7 new-margin "
                    placeholder="Enter new password"
                  />
                </div>
                <div className="row mt-3 mb-5 align-items-center ml-4 border-check">
                  {/* <label>Confirm Password</label> */}
                  <div className="border-check-1">Confirm Password</div>
                  <input
                    name="confirmpassword"
                    type="password"
                    className="form-control col-lg-7 cnfrm-margin "
                    placeholder="Enter confirm pasword"
                  />
                </div>
                <div className="row mt-3 align-items-center ml-4 border-check">
                  {/* <label>Facebook link</label> */}
                  <div className="border-check-1">Facebook Link</div>
                  <input
                    name="job"
                    type="link"
                    className="form-control col-lg-7 facebook-margin "
                    placeholder="Enter your facebook link"
                  />
                </div>
                <div className="row mt-3 align-items-center ml-4 border-check">
                  {/* <label>Twitter link</label> */}
                  <div className="border-check-1">Twitter link</div>
                  <input
                    name="job"
                    type="link"
                    className="form-control col-lg-7 twitter-margin "
                    placeholder="Enter your twitter link"
                  />
                </div>

                <div className="row mt-5 ml-2 border-check ">
                  <div className="col-9 border-check">Email notification</div>
                  <div className="col-3 border-check switch-email-toggle">
                    <label className="toggle">
                      <input
                        className="toggle__input"
                        name
                        type="checkbox"
                        id="myToggle"
                      />
                      <div className="toggle__fill" />
                    </label>
                  </div>
                </div>
                <div className="row mt-3 ml-2 border-check ">
                  <div className="col-9">Video autoplay</div>
                  <div className="col-3">
                    <label className="toggle">
                      <input
                        className="toggle__input"
                        name
                        type="checkbox"
                        id="myToggle"
                      />
                      <div className="toggle__fill" />
                    </label>
                  </div>
                </div>
                <div className="row mt-3 ml-2 border-check ">
                  <div className="col-9">
                    Inform me before showing media that may be sensitive
                  </div>
                  <div className="col-3">
                    <label className="toggle">
                      <input
                        className="toggle__input"
                        name
                        type="checkbox"
                        id="myToggle"
                      />
                      <div className="toggle__fill" />
                    </label>
                  </div>
                </div>
                <div className="row mt-4 justify-content-center">
                  <button type="submit" className=" button-right">
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
